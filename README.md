# GPSCorrection

- PGAdmin III
- PostgreSQL 10.10
- PostGIS 2.4
- Osmosis
- RStudio

## Algorithm
- Java Implementation http://pca.narod.ru/KeglTheJavaImplementationofthePrincipalGraphAlgorithm.htm
- Principal Curve R https://rdrr.io/cran/princurve/man/principal_curve.html
- Principal Curve R (Alternative) https://github.com/rcannood/princurve

![Street mapping](street_mapping.png)

![Point to line distance](point2line.png)