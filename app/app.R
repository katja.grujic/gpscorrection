library(shiny)
library(tidyverse)
library(DT)
library(leaflet)
library(princurve)
library(rgeos)
library(sp)
library(sf)
library(geosphere)
library(fpCompare)
library(rpostgis)
library(tidyverse)

wgs.84 <- "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"
htrs.96 <- "+proj=tmerc +lat_0=0 +lon_0=16.5 +k=0.9999 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"

# set telorance value
tol = .Machine$double.eps^0.5       # default value
options(fpCompare.tolerance = tol)
#read_csv("staza1.csv") %>% select(longitude, latitude) -> data
#pcurve <- principal_curve(as.matrix(data), smoother="smooth_spline", all.knots = T, spar = 1, stretch = 0.1)




ui <- fluidPage(
  titlePanel(p("Spatial app", style = "color:#3474A7")),
  sidebarLayout(
    sidebarPanel(
      fileInput(
        inputId = "filedata",
        label = "Upload data. Choose csv file",
        accept = c(".csv")
      )
    ), 
    
    mainPanel(
      leafletOutput(outputId = "map"),
      DTOutput(outputId = "table")
    )
  )
)

getColor<-function(value){
  
  if(value <= 0.5){
    
    return("blue")
    
  }
  else if(value > 0.5 & value <= 1 ){
    
    return("darkgreen")
    
  }
  else{
    
    return("red")
    
  }
  
  return("red")
  
}

server <- function(input, output){
  data <- reactive({
    req(input$filedata)
    read_csv(input$filedata$datapath) %>% select(longitude, latitude)
  })
  output$table <- renderDT({
    data <- data()
    if (is_null(data)) {
      return(NULL)
    }
    
    
    
    
    data <- data()
    pcurve <- principal_curve(as.matrix(data), smoother="smooth_spline", all.knots = T, spar = 1, stretch = 0.1) 
    #print(data)
    data.sp <- SpatialPoints(as.matrix(data), CRS(wgs.84))
    
    pcurve.sp <- Line(as.matrix(pcurve$s))
    pcurve.sp <- Lines(list(pcurve.sp), ID = "a")
    pcurve.sp <- SpatialLines(list(a = pcurve.sp), proj4string = CRS(wgs.84))
    
    data.sp <-spTransform(data.sp, CRS(htrs.96))
    pcurve.sp <-spTransform(pcurve.sp, CRS(htrs.96))
    
    data$v <- numeric(length(data.sp))
    
    for (i in 1:length(data.sp)) data$v[i] <- gDistance(data.sp[i], pcurve.sp)
    
    
    return(data)
  }, colnames = c("Geografska dužina", "Geografska širina", "Udaljenost"), rownames = F , filter = 'none', escape = F, options = list(dom = 'ltp',  columnDefs = list(list(className = 'dt-center', targets = '_all'))))
  output$map <- renderLeaflet({
    if (is_null(data())) {
      return(NULL)
    }
    
    
    data <- data()
    pcurve <- principal_curve(as.matrix(data), smoother="smooth_spline", all.knots = T, spar = 1, stretch = 0.1) 
    
    pcurve.sp  <- SpatialPoints(as.matrix((pcurve$s)), CRS(wgs.84))
    data.sp <- SpatialPoints(as.matrix(data), CRS(wgs.84))
    
    
    
    data$v <- numeric(length(data.sp))
    closest <- matrix(numeric(), nrow = length(data.sp), ncol = 2)
    for (i in 1:length(data.sp)){
      d <- dist2Line(data.sp[i], pcurve.sp)
      
      data$v[i] <- d[1]
      
      closest[[i,1]] <- d[2]
      closest[[i,2]] <- d[3]
      
    }  
    
    closest.sp <- SpatialPoints(as.matrix(closest), CRS(wgs.84))
    
    max.point <- data[c(366, 281, 280), ]
    
    vector.values <- c()
    for(i in 1:length(data$v)) vector.values[i] <- data$v[i]
    vector.colors <- c()
    for(i in 1:length((vector.values))){
      
      vector.colors[i] = getColor(vector.values[i])
      
      
    }
    
    
    #DATAFRAME
    mydf <- data.frame(
      InitialLat = c(data.sp$latitude),
      InitialLong = c(data.sp$longitude),
      NewLat = c(closest.sp$coords.x2),
      NewLong = c(closest.sp$coords.x1),
      stringsAsFactors = FALSE)
    
    mydf2 <- data.frame(
      lat = c(mydf$InitialLat, mydf$NewLat),
      long = c(mydf$InitialLong, mydf$NewLong)
    )
    
    pal <- colorNumeric(
      palette = vector.colors,
      domain = vector.values
    )
    
    legend.values = c(0, 0.5, 1, Inf)
    legend.colors = c("blue","darkgreen","red")
    
    qpal <- colorBin(legend.colors, data$v,  bins = legend.values)
    
    map <- leaflet(data) %>% 
      addTiles() %>%
      addProviderTiles(providers$CartoDB.Positron, options = providerTileOptions(minZoom = 15, maxZoom = 30)) %>%
      addCircleMarkers(lat = data$latitude, lng = data$longitude, radius = 2, fillOpacity = 1, stroke = F, color = "blue") %>%
      addPolylines(lat = pcurve$s[, 2], lng = pcurve$s[, 1], color = "red", weight = 2, opacity = 1)  #%>%
      #addLegend(pal = qpal, values = ~legend.values, opacity = 1, title = "Udaljenosti")
    
    for(i in 1:length(data.sp$latitude)){
      
      dataFrame <- data.frame(
        lat = c(data.sp$latitude[i], closest.sp$coords.x2[i]),
        long = c(data.sp$longitude[i], closest.sp$coords.x1[i])
      )
      
      map <- addPolylines(map, data = dataFrame, lng = ~long, lat = ~lat, color = "black", weight = 1, opacity = 1)
      
    }
    
    map
  })
  
}

shinyApp(ui = ui, server = server)